import java.util.Scanner;

public class Romzahl {

    public static void main(String[] args) {
        System.out.print("Bitte R�mische Zahl eingeben: ");
        String input = new Scanner(System.in).next().trim(); 
        System.out.println();
        int result = parse(input);
        System.out.println(result);
    }

    static int parse(String in) {
        int out = 0;
        final int[] numerals = in.chars() // Nimm dir die chars in dem string (also die r�mischen Ziffern)
                .map(Romzahl::toValue) // Mach aus jeder Ziffer den Wert der Ziffer. (also 'X' -> 10 etc.)
                .toArray(); // Pack alles in ein Array

        for (int i = 0; i < numerals.length; ) {
            final int current = numerals[i];

            // Wenn wir am letzten index des Array sind
            if (i == numerals.length - 1) {
                out += current;
                break;
            }

            final int next = numerals[i + 1];

            // Wenn die r�mische Ziffer jetzt von der n�chsten abgezogen darf:
            if (allowedSubtraction(next) == current) {
                out += next; // Die n�chste Ziffer wird draufaddiert
                out -= current; // Die jetzige Ziffer wird abgezogen
                i += 2; // wir springen zur �bern�chsten Ziffer (wir haben die n�chste ja schon draufaddiert)
                continue;
            }

            // Sonst addieren wir einfach drauf
            out += current;
            i++;
        }
        return out;
    }

    /**
     * Wandelt eine r�mische Ziffer in ihren Wert um. Die Funktion nimmt ein int statt einem char, weil das in Z.15 f�r map gebraucht wird
     * Theoretisch w�rde die Funktion aber auch funktionieren, wenn romanDigit ein char is
     */
    static int toValue(int romanDigit) {
        return switch (Character.toUpperCase(romanDigit)) {
            case 'I' -> 1;
            case 'V' -> 5;
            case 'X' -> 10;
            case 'L' -> 50;
            case 'C' -> 100;
            case 'D' -> 500;
            case 'M' -> 1000;
            default  -> {
                throw new IllegalArgumentException("Invalid Roman Numeral");
            }
        };
    }

    /**
     * Gibt den Wert derjenigen R�mischen Ziffer zur�ck, die man von der Ziffer mit dem gegebenen Wert abziehen darf
     * z.B. allowedSubtraction(50) -> 10, weil nur man X (10) von L (50) abziehen darf
     */
    static int allowedSubtraction(int value) {
        return switch (value) {
            case 5,   10    -> 1;
            case 50,  100   -> 10;
            case 500, 1000  -> 100;
            default         -> {
                throw new IllegalArgumentException("Invalid Roman Numeral Value");
            }
        };
    }
}