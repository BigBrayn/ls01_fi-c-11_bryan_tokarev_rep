import java.util.Scanner;

public class MittelwertMitMethoden {
	
	public static double eingabe(Scanner myScanner, String text) {
		System.out.println(text);
		double result = myScanner.nextDouble();
		System.out.println("Ihre Eingabe war:"+result);
		return result;
		
	}
	
	public static double berechnung(Scanner myScanner, double[] arr, int arraysize) {
		double mittelwert = 0;
		for(int i=0; i<arr.length; i++) {
			mittelwert += arr[i];
		}
		mittelwert = mittelwert/arraysize;
		return mittelwert;
	}
	
	public static void ausgabe(double mittelwert) {
		System.out.println("Der Mittelwert ist:" +mittelwert);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Von wie vielen Zahlen wollen sie den Mittelwert bilden?" );
		int arraysize = myScanner.nextInt();
		double[] arr = new double[arraysize];
		arr[0] = eingabe(myScanner, "Bitte geben Sie die erste Zahl ein: ");
		for(int i=1; i<arraysize; i++) {
			arr[i] = eingabe(myScanner,"Geben sie eine weitere Zahl ein");	
		}
		double mittelwert=berechnung(myScanner, arr, arraysize);
		ausgabe(mittelwert);	

	}

}
