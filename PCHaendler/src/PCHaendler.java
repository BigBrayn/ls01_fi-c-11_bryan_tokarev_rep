import java.util.Scanner;

public class PCHaendler {
	
	public static String liesString(Scanner myScanner, String text) {
		System.out.println(text);
		String s = myScanner.next();
		return s;
	}
	
	public static int liesInt(Scanner myScanner, String text) {
		System.out.println(text);
		int x = myScanner.nextInt();
		return x;
	}
	
	public static double liesDouble(Scanner myScanner, String text) {
		System.out.println(text);
		double y = myScanner.nextDouble();
		return y;
	}
	
	public static double berechneGesamtNettopreis(int anzahl, double nettopreis) {
		double preis = anzahl*nettopreis;
		return preis;
	}
	
	public static double berechneGesamtBruttopreis(double gesamtnettopreis, double mwst) {
		double bruttogesamtpreis = gesamtnettopreis * (1 + mwst / 100);
		return bruttogesamtpreis;
	}
	
	public static void rechnungausgeben(String artikel, int anzahl, double
			nettogesamtpreis, double bruttogesamtpreis, double mwst) {
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.2f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
		
	}

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);

		// Benutzereingaben lesen
		String artikel = liesString(myScanner, "was m�chten Sie bestellen?");
		
		int anzahl = liesInt(myScanner, "Geben Sie die Anzahl ein:");

		double preis = liesDouble(myScanner, "Geben Sie den Nettopreis ein:");
		
		double mwst = liesDouble(myScanner, "Geben Sie den Mehrwertsteuersatz in Prozent ein:");

		// Verarbeiten
		double nettogesamtpreis = berechneGesamtNettopreis(anzahl, preis);
		double bruttogesamtpreis = berechneGesamtBruttopreis(nettogesamtpreis,mwst);

		// Ausgeben

		rechnungausgeben(artikel, anzahl, nettogesamtpreis, bruttogesamtpreis, mwst);

	}

}