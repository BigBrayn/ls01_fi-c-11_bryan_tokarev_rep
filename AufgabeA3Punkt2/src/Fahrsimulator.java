import java.util.Scanner;

public class Fahrsimulator {
	
	public static double beschleunige(double v, double dv) {
		v=v+dv;
		if (v<0) {
			v=0;
		}
		if (v>130) {
			v=130;
		}
		return v;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner myScanner = new Scanner(System.in);
		double v = 0.0;
		boolean wd=true;
		do {
			System.out.println("Um wie viel soll die Geschwindigkeit ge�ndert werden? Positiver Wert = Beschleunigen, negativer Wert = Bremsen. Sobald sie die Geschwindigkeit 111km/h erreichen, bricht das Programm ab.");
			double dv = myScanner.nextDouble();
			v=beschleunige(v,dv);
			System.out.println("Neue Geschwindigkeit:"+v+"km/h");
			if(v==111) {
				wd=false;
			}
		}while(wd);

	}

}
