import java.util.Scanner;

public class MethodenRechenuebung {
	
	public static double reihenschaltung(double r1, double r2) {
		double re = r1 + r2;
		return re;
	}
	
	public static double parallelschaltung(double r1, double r2) {
		double re = (r1*r2)/(r1+r2);
		return re;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Wie gro� ist der erste Widerstand R1? Bitte geben sie nur die Zahl an.");
		double r1 = myScanner.nextDouble();
		System.out.println("Der erste Widerstand ist also " +r1 +"Omen gro�.");
		System.out.println("Wie gro� ist der zweite Widerstand R2? Wieder bitte nur die Zahl angeben.");
		double r2 = myScanner.nextDouble();
		System.out.println("Der zweite Widerstand R2 ist also "+r2+"Omen gro�.");
		double re= reihenschaltung(r1,r2);
		System.out.println("Damit ist der gesamt Widerstand RE "+re+"Omen gro�, wenn sie in Reihe geschalten sind.");
		re=parallelschaltung(r1,r2);
		System.out.println("Bei einer Parallelschaltung betr�gt RE jedoch "+re+"Omen.");

	}

}
