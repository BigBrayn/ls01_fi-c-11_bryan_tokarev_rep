import java.util.Scanner;

public class Mathe {
	
	public static double quadrat(double x) {
		return (x*x);
	}
	public static double hypotenuse(double k1, double k2) {
		double h = Math.sqrt(quadrat(k1)+quadrat(k2));
		return h;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner myScanner = new Scanner(System.in);
//		System.out.println("Welche Zahl soll quadriert werden?");
//		double x = myScanner.nextDouble();
//		System.out.println("Ergebnis: "+ quadrat(x));
		double k1 = 12;
		double k2 = 14;
		double h = hypotenuse(k1,k2);
		System.out.println(h);
	}

}
