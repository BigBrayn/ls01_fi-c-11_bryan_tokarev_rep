
public class Konsolenausgabe {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//Erste Versuche mit print und println
		System.out.println("Hello World");
		System.out.print("Hello\n");
		System.out.println("I had to redo this because it didn't work.");
		System.out.println("\"Never use your google account\" @ Marty");
		
		//Test: einf�gen von variablen
		int alter = 22;
		String name = "Bryan";
		System.out.println("Ich bin " + name + " und bin " + alter + " Jahre alt");
		
		//Test: Konsolenausgabe printf
		
		System.out.printf("Hello %s!%n", "World");
		
		//Formatierung von String
		String s = "TSV Spandau 1860";
		
		//einfache Ausgabe
		System.out.printf("\n|%s|\n", s);
		
		//formatierung gleicher String
		System.out.printf( "|%-16.11s|\n\n", s);	
		
		//Apology
		System.out.println("oops, Ich habe viel rumprobiert und die Aufgaben �bersehen, die kommen jetzt hier drunter");
		
		//Aufgabe 1
		String s2 = "Satz";
		
		System.out.print("Das ist der erste " + s2 + ".\n");
		System.out.println("Direkt \"folgt\" der zweite " + s2 + ".\n");
		//Aufgabe 1 Fertig
		
		//Beginn Aufgabe 2
		String p = "*************";
		
		System.out.printf("%7.1s\n", p);
		System.out.printf("%8.3s\n", p);
		System.out.printf("%9.5s\n", p);
		System.out.printf("%10.7s\n", p);
		System.out.printf("%11.9s\n", p);
		System.out.printf("%12.11s\n", p);
		System.out.printf("%13.13s\n", p);
		System.out.printf("%8.3s\n", p);
		System.out.printf("%8.3s\n\n", p);
		//Ende Aufgabe 2.
		
		//Aufgabe 3
		double d1 = 22.4234234;
		double d2 = 111.2222;
		double d3 = 4.0;
		double d4 = 1000000.551;
		double d5 = 97.34;
		
		System.out.printf("%.2f \n%.2f \n%.2f \n%.2f \n%.2f \n", d1, d2, d3, d4, d5);
		//Aufgabe 3 beendet

	}

}
