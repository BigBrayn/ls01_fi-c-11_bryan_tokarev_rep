import java.util.Scanner;

class Fahrkartenautomat
{
	
	public static double bestellungErfassen(Scanner tastatur) {
		double zuZahlenderBetrag = 0;
		short anzahlTickets;
		boolean check = true;
		boolean ticketerror = false;
		double ticketeinzelpreis = 0;
		String eingabe;
		do {
				//Ticketarten 
		do {
			System.out.println("Es gibt A (1.40�), B(1,60�), C(1�), AB(2,60�), BC(2,60�), ABC(3,50�) Tickets. Welche Art wollen sie erwerben?");
			eingabe = tastatur.next();
		if(!eingabe.equals("A") && !eingabe.equals("B") && !eingabe.equals("C") && !eingabe.equals("AB") && !eingabe.equals("BC") && !eingabe.equals("ABC")) {
			System.out.println("Error! Diese Ticketart gibt es leider nicht. Achten sie auf Gro�- und Kleinschreibung.");
			ticketerror = true;
		}
		} while (ticketerror);
		if(eingabe.equals("A")) {
			ticketeinzelpreis = 1.4;
		}
		if(eingabe.equals("B")) {
			ticketeinzelpreis = 1.6;
		}
		if(eingabe.equals("C")) {
			ticketeinzelpreis = 1;
		}
		if(eingabe.equals("AB")) {
			ticketeinzelpreis = 2.6;
		}
		if(eingabe.equals("BC")) {
			ticketeinzelpreis = 2.6;
		}
		if(eingabe.equals("ABC")) {
			ticketeinzelpreis = 3.5;
		}
		System.out.print("Wie viele Tickets dieser Art m�chten sie erwerben? Sie k�nnen maximal 10 Tickets erwerben: ");
		anzahlTickets = tastatur.nextShort();
		if(anzahlTickets<1 || anzahlTickets>10) {
			anzahlTickets=1;
			System.out.println("Ung�ltige Eingabe! Ihre Ticketanzahl wurde auf 1 gesetzt.");
		}
		zuZahlenderBetrag = ticketeinzelpreis * anzahlTickets + zuZahlenderBetrag;
		System.out.printf("Derzeitiger Preis: %.2f�%n", zuZahlenderBetrag);
		System.out.println("M�chten sie noch weitere Tickets erwerben? 1 - ja; 2 - Nein");
		if(tastatur.nextInt()==2) {
			check=false;
		}
		} while(check);
		return zuZahlenderBetrag;
	}
	
	public static double bezahlen(double zuZahlenderBetrag, Scanner tastatur, double eingezahlterGesamtbetrag) {
		System.out.printf("Noch zu zahlen: %.2f�%n", (zuZahlenderBetrag - eingezahlterGesamtbetrag)); //Ausgabe x,xx� und Zeilenumbruch
 	    System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
 	    double  eingeworfeneM�nze = tastatur.nextDouble();
        eingezahlterGesamtbetrag += eingeworfeneM�nze;
        return eingezahlterGesamtbetrag;
	}
	
	public static void ausgabe() {
		 System.out.println("\nFahrschein wird ausgegeben");
	       for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
	          try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	       }
	       System.out.println("\n\n");
	}
	
	public static void rueckgeld(double rueckbetrag) {
		  System.out.printf("Der R�ckbetrag in H�he von %.2f� wird in folgenden M�nzen ausgezahlt:\n", rueckbetrag);
		  final double[] muenzen = {2, 1, 0.5, 0.2, 0.1, 0.05, 0.02, 0.01};
		  final double eps = 0.00001; // Um mit Rundungsfehlern zu helfen
		  for (int i = 0; i < muenzen.length;) {
		    if(rueckbetrag + eps >= muenzen[i]) {
		      if (i < 2) { // wenn wir also bei euro m�nzen sind
		        System.out.println(muenzen[i] + " EURO");
		      } else {
		        System.out.println(muenzen[i] * 100 + " CENT");
		      }
		      rueckbetrag -= muenzen[i];
		    } else {
		      i++;
		    }
		  }
		}
	
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
       
       
       
    
       //deklarierte Variablen au�er i
       double zuZahlenderBetrag; 
       double eingezahlterGesamtbetrag;
       double rueckgabebetrag; //neu hinzugef�gt, bestimmt nicht so viele tickets also short, aber evtl zum testen �ber 27, sonst byte, wird multipliziert mit dem eingegebenen Einzelpreis

       zuZahlenderBetrag= bestellungErfassen(tastatur); //Der vorher eingegebene Betrag des einzelnen Tickets wird vom Speicher abgerufen und wird mit der Anzahl der Tickets multipliziert. Der errechnete Wert ersetzt auf dem Arbeitsspeicher den alten Wert. 

       // Geldeinwurf
       // -----------
       eingezahlterGesamtbetrag = 0.0;
       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
       {
    	   eingezahlterGesamtbetrag = bezahlen(zuZahlenderBetrag, tastatur, eingezahlterGesamtbetrag);
       }

       // Fahrscheinausgabe
       // -----------------
       ausgabe();

       // R�ckgeldberechnung und -Ausgabe
       // -------------------------------
      // System.out.println(eingezahlterGesamtbetrag+"\n"+zuZahlenderBetrag); Test f�r R�ckgabewert gewesen
       rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
       if(rueckgabebetrag > 0.0)
       {
    	  rueckgeld(rueckgabebetrag);
       }

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir w�nschen Ihnen eine gute Fahrt.");
       tastatur.close();
    }
}