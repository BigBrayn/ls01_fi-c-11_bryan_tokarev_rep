
public class Konsolenausgabe2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//Start Aufgabe 1 �bung 2
		String s = "**";
				
		System.out.printf("%6s\n", s);
		System.out.printf("%2.1s%7.1s\n", s, s);
		System.out.printf("%2.1s%7.1s\n", s, s);
		System.out.printf("%6s\n\n", s);
		
		//Aufgabe 6 beendet
		//gerade kein Internet verf�gbar f�r push ins git
		
		//start Aufgabe 2
		
		int n = 5;
		
		System.out.printf("%-5s", n-5 + "!");
		System.out.print("= ");
		System.out.printf("%19s", " ");
		System.out.print("= ");
		System.out.printf("%4s\n", "1");
		System.out.printf("%-5s", n-4 + "!");
		System.out.print("= ");
		System.out.printf("%-19s", n-4);
		System.out.print("= ");
		System.out.printf("%4s\n", "1");
		System.out.printf("%-5s", n-3 + "!");
		System.out.print("= ");
		System.out.printf("%-19s", "1 * 2");
		System.out.print("= ");
		System.out.printf("%4s\n", "2");
		System.out.printf("%-5s", n-2 + "!");
		System.out.print("= ");
		System.out.printf("%-19s", "1 * 2 * 3");
		System.out.print("= ");
		System.out.printf("%4s\n", "6");
		System.out.printf("%-5s", n-1 + "!");
		System.out.print("= ");
		System.out.printf("%-19s", "1 * 2 * 3 * 4");
		System.out.print("= ");
		System.out.printf("%4s\n", "24");
		System.out.printf("%-5s", n + "!");
		System.out.print("= ");
		System.out.printf("%-19s", "1 * 2 * 3 * 4 * 5");
		System.out.print("= ");
		System.out.printf("%4s\n", "120");
		
		//Ende Aufgabe 2 
		
		//Anfang Aufgabe 3
		
		int F1 = -20;
		int F2 = -10;
		int F3 = 0;
		int F4 = 20;
		int F5 = 30;
		double C1 = -28.8889;
		double C2 = -23.3333;
		double C3 = -17.7778;
		double C4 = -6.6667;
		double C5 = -1.1111;
		
		System.out.printf("\n%-12s", "Fahrenheit");
		System.out.print(" | ");
		System.out.printf("%10s\n", "Celsius");
		System.out.printf("%25s\n", "-------------------------");
		System.out.printf("%+-12d", F1);
		System.out.print(" | ");
		System.out.printf("%10.2f\n", C1);
		System.out.printf("%+-12d", F2);
		System.out.print(" | ");
		System.out.printf("%10.2f\n", C2);
		System.out.printf("%+-12d", F3);
		System.out.print(" | ");
		System.out.printf("%10.2f\n", C3);
		System.out.printf("%+-12d", F4);
		System.out.print(" | ");
		System.out.printf("%10.2f\n", C4);
		System.out.printf("%+-12d", F5);
		System.out.print(" | ");
		System.out.printf("%10.2f\n", C5);

	}

}
