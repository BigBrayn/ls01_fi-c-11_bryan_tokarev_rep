
public class Temperaturtabelle {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//Variablen deklarieren
		int F1 = -20;
		int F2 = -10;
		int F3 = 0;
		int F4 = 20;
		int F5 = 30;
		double C1 = -28.8889;
		double C2 = -23.3333;
		double C3 = -17.7778;
		double C4 = -6.6667;
		double C5 = -1.1111;
		
		//Kopfzeile
		System.out.printf("\n%-12s", "Fahrenheit");
		System.out.print(" | ");
		System.out.printf("%10s\n", "Celsius");
		System.out.printf("%25s\n", "-------------------------"); //Trennung Kopfzeile von Tabelle
		System.out.printf("%+-12d", F1); //Tabelleninhalt auflisten, Vorzeichen anzeigen lassen
		System.out.print(" | ");
		System.out.printf("%10.2f\n", C1); //Zwei nachkommastellen anzeigen lassen, Zeilenumbruch
		System.out.printf("%+-12d", F2); //ganz viel copy and paste
		System.out.print(" | ");
		System.out.printf("%10.2f\n", C2);
		System.out.printf("%+-12d", F3);
		System.out.print(" | ");
		System.out.printf("%10.2f\n", C3);
		System.out.printf("%+-12d", F4);
		System.out.print(" | ");
		System.out.printf("%10.2f\n", C4);
		System.out.printf("%+-12d", F5);
		System.out.print(" | ");
		System.out.printf("%10.2f\n", C5);

	}

}
